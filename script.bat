@ECHO Off
SETLOCAL

FOR /F "tokens=* USEBACKQ" %%F IN (`reg query HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize /v AppsUseLightTheme`) DO (
    SET var=%%F
)
ECHO %var:~-1%
IF "%var:~-1%"=="0" (
    reg add HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize /v AppsUseLightTheme /t REG_DWORD /d 1 /f
) else (
    reg add HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize /v AppsUseLightTheme /t REG_DWORD /d 0 /f
)